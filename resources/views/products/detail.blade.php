@extends('template')
@section('content')
<div class="lg:w-4/5 mx-auto flex flex-wrap">
    @if(Auth::check())
        <div class="w-full text-right px-4">
            <a class="px-4 py-1 text-white font-light tracking-wider rounded text-white bg-teal-500 border-0 focus:outline-none hover:bg-teal-600 text-lg"
                href="{{ route('products.edit', $product->id) }}">Редактировать</a>
        </div>
    @endif
    <img alt="ecommerce" class="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded" src="{{ !empty($product->image_url) ? $product->image_url : 'https://dummyimage.com/400x400' }}">
    <div class="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
        @if(!empty($product->brand))<h2 class="text-sm title-font text-gray-600 tracking-widest">{{ $product->brand }}</h2>@endif
        <h1 class="text-white text-3xl title-font font-medium mb-1">{{ $product->name }}</h1>
        <h2 class="text-sm title-font text-gray-600 tracking-widest">Осталось: {{ $product->count }}</h2>
        @if(!empty($product->brand))<p class="leading-relaxed">{{ $product->description }}</p>@endif
        <div class="flex">
            <span class="title-font font-medium text-2xl text-white">${{ $product->price }}</span>
            <button class="flex ml-auto text-white bg-teal-500 border-0 py-2 px-6 focus:outline-none hover:bg-teal-600 rounded">Купить</button>
        </div>
    </div>
</div>
@endsection