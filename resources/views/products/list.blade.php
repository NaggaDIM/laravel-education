@extends('template')
@section('content')
    <div>
        @if(Auth::check())<div class="w-full text-right px-4"><a class="px-4 py-1 text-white font-light tracking-wider rounded text-white bg-teal-500 border-0 focus:outline-none hover:bg-teal-600 text-lg"
                    href="{{ route('products.add') }}">Добавить</a></div>
        @endif
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
            @if(!empty($products))
                @foreach($products as $product)
                    <div class="p-4">
                        <a href="/products/{{ $product->id }}" class="block relative h-48 rounded overflow-hidden">
                            <img alt="ecommerce" class="object-cover object-center w-full h-full block" src="{{ !empty($product->image_url) ? $product->image_url : 'https://dummyimage.com/420x260' }}">
                        </a>
                        <div class="mt-4">
                            @if(!empty($product->category))<h3 class="text-gray-500 text-xs tracking-widest title-font mb-1">{{ $product->category }}</h3>@endif
                            <h2 class="text-white title-font text-lg font-medium"><a href="/products/{{ $product->id }}">@if(!empty($product->brand)){{ $product->brand }} @endif{{ $product->name }}</a></h2>
                            <p class="mt-1">${{ $product->price }}</p>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="w-full text-center">{{ $products->links() }}</div>
    </div>
@endsection