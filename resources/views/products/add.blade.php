@extends('template')
@section('content')
<div class="w-full lg:w-4/5 mx-auto">
    <form class="w-full p-10" method="post">
        @csrf
        @if(isset($product))<input id="_method" type="hidden" name="_method" value="put">@endif
        <p class="text-white font-medium">{{ isset($product) ? 'Редактирование товара' : 'Добавление товара' }}</p>
        @if(isset($prefer))<input id="prefer" name="prefer" type="hidden" value="{{ $prefer }}">@endif
        <div>
            <label class="block text-sm text-gray-300" for="brand">Брэнд</label>
            <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                   id="brand" name="brand" type="text" aria-label="brand"
                    @if(isset($product))value="{{ $product->brand }}"@endif>
            @if($errors->has('brand'))<p class="text-sm text-red-600">{{ $errors->first('brand') }}</p>@endif
        </div>
        <div class="mt-2">
            <label class="block text-sm text-gray-300" for="name">Название</label>
            <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                   id="name" name="name" type="text" aria-label="name"
                   @if(isset($product))value="{{ $product->name }}"@endif>
            @if($errors->has('name'))<p class="text-sm text-red-600">{{ $errors->first('name') }}</p>@endif
        </div>
        <div class="mt-2">
            <label class="block text-sm text-gray-300" for="description">Описание</label>
            <textarea class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                   id="description" name="description" aria-label="description" rows="5">@if(isset($product)){{ $product->description }}@endif</textarea>
            @if($errors->has('description'))<p class="text-sm text-red-600">{{ $errors->first('description') }}</p>@endif
        </div>
        <div class="mt-2">
            <label class="block text-sm text-gray-300" for="category">Категория</label>
            <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                   id="category" name="category" type="text" aria-label="category"
                   @if(isset($product))value="{{ $product->category }}"@endif>
            @if($errors->has('category'))<p class="text-sm text-red-600">{{ $errors->first('category') }}</p>@endif
        </div>
        <div class="mt-2">
            <label class="block text-sm text-gray-300" for="image_url">Картинка</label>
            <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                   id="image_url" name="image_url" type="text" aria-label="image_url"
                   @if(isset($product))value="{{ $product->image_url }}"@endif>
            @if($errors->has('image_url'))<p class="text-sm text-red-600">{{ $errors->first('image_url') }}</p>@endif
        </div>
        <div class="mt-2">
            <label class="block text-sm text-gray-300" for="count">Количество</label>
            <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                   id="count" name="count" type="text" aria-label="count"
                   @if(isset($product))value="{{ $product->count }}"@endif>
            @if($errors->has('count'))<p class="text-sm text-red-600">{{ $errors->first('count') }}</p>@endif
        </div>
        <div class="mt-2">
            <label class="block text-sm text-gray-300" for="price">Цена</label>
            <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                   id="price" name="price" type="text" aria-label="price"
                   @if(isset($product))value="{{ $product->price }}"@endif>
            @if($errors->has('price'))<p class="text-sm text-red-600">{{ $errors->first('price') }}</p>@endif
        </div>
        <div class="mt-4">
            <button class="px-4 py-1 text-white font-light tracking-wider rounded text-white bg-teal-500 border-0 focus:outline-none hover:bg-teal-600 text-lg" type="submit">{{ isset($product) ? 'Сохранить' : 'Добавить' }}</button>
        </div>
    </form>
</div>
@endsection