<header class="text-gray-500 bg-gray-900 body-font">
    <div class="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
        <a class="flex title-font font-medium items-center text-white mb-4 md:mb-0">
            <span class="ml-3 text-xl">Laravel Education</span>
        </a>
        <nav class="md:mr-auto md:ml-4 md:py-1 md:pl-4 md:border-l md:border-gray-700	flex flex-wrap items-center text-base justify-center">
            <a href="/" class="mr-5 hover:text-white">Главная</a>
            @foreach(\App\Models\Menu::get() as $menuItem)
                {!! $menuItem->render() !!}
            @endforeach
        </nav>
        @if(Auth::check())
            <div id="user" class="w-auto p-2">
                <a href="{{ route('auth.logout') }}" class="mr-5 hover:text-white">{{ Auth::user()->name }}</a>
            </div>
        @else
            <div id="login">
                <form class="w-auto p-2" method="post" action="{{ route('auth.login') }}">
                    @csrf
                    <input class="w-13 px-1 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                           id="email" name="email" type="text" aria-label="email" placeholder="E-mail">
                    <input class="w-13 px-1 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                           id="password" name="password" type="password" aria-label="password" placeholder="Password">
                    <button class="px-4 py-1 text-white font-light tracking-wider rounded text-white bg-teal-500 border-0 focus:outline-none hover:bg-teal-600 text-lg"
                            type="submit">Вход</button>
                </form>
            </div>
        @endif
    </div>
</header>