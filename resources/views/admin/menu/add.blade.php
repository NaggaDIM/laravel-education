@extends('template')
@section('content')
    <main class="flex-1 overflow-x-hidden overflow-y-auto">
        <div class="container mx-auto px-6 py-8">
            <h3 class="text-white text-3xl font-medium">{{ isset($title) ? $title : null }}</h3>

            <div class="flex flex-col mt-4">
                <form class="w-full p-10" method="post">
                    @csrf
                    @if(isset($menu_item))
                        <input id="_method" type="hidden" name="_method" value="put">
                        <div>
                            <label class="block text-sm text-gray-300" for="id">ID:</label>
                            <input class="w-full px-5 py-1 text-white bg-gray-700 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                                   id="id" name="id" type="text" aria-label="id" disabled
                                   @if(isset($menu_item))value="{{ $menu_item->id }}"@endif>
                            @if($errors->has('id'))<p class="text-sm text-red-600">{{ $errors->first('id') }}</p>@endif
                        </div>
                    @endif

                    <div class="mt-2">
                        <label class="block text-sm text-gray-300" for="title">Заколовок:</label>
                        <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                               id="title" name="title" type="text" aria-label="title"
                               @if(isset($menu_item))value="{{ $menu_item->title }}"@endif>
                        @if($errors->has('title'))<p class="text-sm text-red-600">{{ $errors->first('title') }}</p>@endif
                    </div>

                    <div class="mt-2">
                        <label class="block text-sm text-gray-300" for="route">Роут:</label>
                        <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                               id="route" name="route" type="text" aria-label="route"
                               @if(isset($menu_item))value="{{ $menu_item->route }}"@endif>
                        @if($errors->has('route'))<p class="text-sm text-red-600">{{ $errors->first('route') }}</p>@endif
                    </div>

                    <div class="mt-2 flex">
                        <input class="text-white mt-1 mr-1"
                               id="is_auth" name="is_auth" type="checkbox" aria-label="is_auth"
                               @if(isset($menu_item) && $menu_item->is_auth)checked="true"@endif>
                        <label class="text-sm text-gray-300" for="is_auth">Только для авторизованых?</label>
                        @if($errors->has('is_auth'))<p class="text-sm text-red-600">{{ $errors->first('is_auth') }}</p>@endif
                    </div>

                    <div class="mt-4">
                        <button class="px-4 py-1 text-white font-light tracking-wider rounded text-white bg-teal-500 border-0 focus:outline-none hover:bg-teal-600 text-lg" type="submit">{{ isset($menu_item) ? 'Сохранить' : 'Добавить' }}</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection