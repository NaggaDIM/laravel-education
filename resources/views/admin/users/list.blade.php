@extends('template')
@section('content')
    <main class="flex-1 overflow-x-hidden overflow-y-auto">
        <div class="container mx-auto px-6 py-8">
            <h3 class="text-white text-3xl font-medium">Пользователи</h3>

            <div class="flex flex-col mt-8">
                <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                    <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-800">
                        <table class="min-w-full">
                            <thead class="bg-gray-800">
                                <tr>
                                    <th class="px-6 py-3 border-b border-gray-700 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">ID</th>
                                    <th class="px-6 py-3 border-b border-gray-700 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Пользователь</th>
                                    <th class="px-6 py-3 border-b border-gray-700 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">API Token</th>
                                    <th class="px-6 py-3 border-b border-gray-700 bg-gray-50 text-right text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                        <a href="{{ route('admin.users.add') }}" class="text-teal-600 hover:text-teal-300">Add</a>
                                    </th>
                                </tr>
                            </thead>

                            <tbody class="bg-gray-900">
                                @foreach($users as $user)
                                    <tr>
                                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-700">
                                            <div class="text-sm leading-5 text-white">{{ $user->id }}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-700">
                                            <div class="text-sm leading-5 text-white">{{ $user->name }}</div>
                                            <div class="text-sm leading-5 text-gray-500">{{ $user->email }}</div>
                                        </td>

                                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-700">
                                            <div class="text-sm leading-5 text-white">{{ $user->api_token }}</div>
                                        </td>

                                        <td class="px-6 py-4 whitespace-no-wrap text-right border-b border-gray-700 text-sm leading-5 font-medium">
                                            <a href="{{ route('admin.users.edit', $user->id) }}" class="text-teal-600 hover:text-teal-300">Edit</a>
                                            <a href="{{ route('admin.users.delete', $user->id) }}" class="text-red-600 hover:text-red-300">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="w-full my-3 px-2">{{ $users->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection