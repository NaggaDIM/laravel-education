@extends('template')
@section('content')
    <main class="flex-1 overflow-x-hidden overflow-y-auto">
        <div class="container mx-auto px-6 py-8">
            <h3 class="text-white text-3xl font-medium">{{ isset($title) ? $title : null }}</h3>

            <div class="flex flex-col mt-4">
                <form class="w-full p-10" method="post">
                    @csrf
                    @if(isset($user))
                        <input id="_method" type="hidden" name="_method" value="put">
                        <div>
                            <label class="block text-sm text-gray-300" for="id">ID:</label>
                            <input class="w-full px-5 py-1 text-white bg-gray-700 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                                   id="id" name="id" type="text" aria-label="id" disabled
                                   @if(isset($user))value="{{ $user->id }}"@endif>
                            @if($errors->has('id'))<p class="text-sm text-red-600">{{ $errors->first('id') }}</p>@endif
                        </div>
                    @endif

                    <div class="mt-2">
                        <label class="block text-sm text-gray-300" for="email">E-mail:</label>
                        <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                               id="email" name="email" type="text" aria-label="email"
                               @if(isset($user))value="{{ $user->email }}"@endif>
                        @if($errors->has('email'))<p class="text-sm text-red-600">{{ $errors->first('email') }}</p>@endif
                    </div>

                    <div class="mt-2">
                        <label class="block text-sm text-gray-300" for="name">Имя:</label>
                        <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                               id="name" name="name" type="text" aria-label="name"
                               @if(isset($user))value="{{ $user->name }}"@endif>
                        @if($errors->has('name'))<p class="text-sm text-red-600">{{ $errors->first('name') }}</p>@endif
                    </div>

                    <div class="mt-2">
                        <label class="block text-sm text-gray-300" for="password">Пароль:</label>
                        <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                               id="password" name="password" type="password" aria-label="password"
                               @if(isset($user) && !empty($user->password))placeholder="Был когда-то введён"@endif>
                        @if($errors->has('password'))<p class="text-sm text-red-600">{{ $errors->first('password') }}</p>@endif
                    </div>

                    <div class="mt-2">
                        <label class="block text-sm text-gray-300" for="api_token">API Token:</label>
                        <input class="w-full px-5 py-1 text-white bg-gray-800 rounded border border-gray-700 focus:outline-none focus:border-teal-500"
                               id="api_token" name="api_token" type="text" aria-label="api_token"
                               @if(isset($user))value="{{ $user->api_token }}"@endif>
                        @if($errors->has('api_token'))<p class="text-sm text-red-600">{{ $errors->first('api_token') }}</p>@endif
                    </div>

                    <div class="mt-4">
                        <button class="px-4 py-1 text-white font-light tracking-wider rounded text-white bg-teal-500 border-0 focus:outline-none hover:bg-teal-600 text-lg" type="submit">{{ isset($user) ? 'Сохранить' : 'Добавить' }}</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection