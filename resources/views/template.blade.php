<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ isset($title) ? $title : null }} | Laravel Education</title>
        <link href="/css/app.css" rel="stylesheet">
    </head>
    <body class="flex flex-col min-h-screen bg-gray-900 body-font">
        @include('_includes.header')
        <main class="flex-grow">
            <section class="text-gray-500">
                <div class="container px-2 py-2 mx-auto">
                    @yield('content')
                </div>
            </section>
        </main>
        {{--FOOTER--}}
        @include('_includes.footer')
    </body>
</html>