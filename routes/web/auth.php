<?php
/**
 * @Author Dmitry Smertin Aka NaggaDIM (naggadim.18@gmail.com)
 * @link https://vk.com/naggadim
 * Date: 24.09.20
 * Time: 10:38
 *
 * Если этот код работает, его написал NaggaDIM,
 * а если нет, то не знаю, кто его писал.
 */
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::get('/logout', [AuthController::class, 'logout'])->middleware('auth')->name('logout');
});