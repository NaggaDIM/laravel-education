<?php
/**
 * @Author Dmitry Smertin Aka NaggaDIM (naggadim.18@gmail.com)
 * @link https://vk.com/naggadim
 * Date: 24.09.20
 * Time: 10:40
 *
 * Если этот код работает, его написал NaggaDIM,
 * а если нет, то не знаю, кто его писал.
 */
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\MainController;

Route::group(['prefix' => '/admin', 'as' => 'admin.', 'middleware' => ['auth']], function (){
    Route::get('/', [MainController::class, 'index'])->name('index');
    Route::group(['prefix' => '/users', 'as' => 'users.'], function (){
        Route::get('/', [MainController::class, 'users'])->name('list');
        Route::get('/add', [MainController::class, 'add_user'])->name('add');
        Route::post('/add', [MainController::class, '_add_user']);
        Route::get('/{id}/edit', [MainController::class, 'edit_user'])->where('id', '[0-9]+')->name('edit');
        Route::put('/{id}/edit', [MainController::class, '_edit_user'])->where('id', '[0-9]+');
        Route::get('/{id}/delete', [MainController::class, '_delete_user'])->where('id', '[0-9]+')->name('delete');
    });

    Route::group(['prefix' => '/products', 'as' => 'products.'], function (){
        Route::get('/', [MainController::class, 'products'])->name('list');
        Route::get('/{id}/delete', [MainController::class, '_delete_product'])->where('id', '[0-9]+')->name('delete');
    });

    Route::group(['prefix' => '/menu', 'as' => 'menu.'], function (){
        Route::get('/', [MainController::class, 'menu'])->name('list');
        Route::get('/add', [MainController::class, 'add_menu'])->name('add');
        Route::post('/add', [MainController::class, '_add_menu']);
        Route::get('/{id}/edit', [MainController::class, 'edit_menu'])->where('id', '[0-9]+')->name('edit');
        Route::put('/{id}/edit', [MainController::class, '_edit_menu'])->where('id', '[0-9]+');
        Route::get('/{id}/delete', [MainController::class, '_delete_menu'])->where('id', '[0-9]+')->name('delete');
    });
});