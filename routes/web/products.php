<?php
/**
 * @Author Dmitry Smertin Aka NaggaDIM (naggadim.18@gmail.com)
 * @link https://vk.com/naggadim
 * Date: 24.09.20
 * Time: 10:40
 *
 * Если этот код работает, его написал NaggaDIM,
 * а если нет, то не знаю, кто его писал.
 */
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;

Route::group(['prefix' => '/products', 'as' => 'products.'], function(){
    Route::get('/', [ProductsController::class, 'index'])->name('list');
    Route::get('/{id}', [ProductsController::class, 'detail'])->where('id', '[0-9]+')->name('detail');
    Route::group(['middleware' => 'auth'], function (){
        Route::get('/add', [ProductsController::class, 'add'])->name('add');
        Route::post('/add', [ProductsController::class, '_add']);
        Route::get('/{id}/edit', [ProductsController::class, 'edit'])->where('id', '[0-9]+')->name('edit');
        Route::put('/{id}/edit', [ProductsController::class, '_edit'])->where('id', '[0-9]+');
    });
});