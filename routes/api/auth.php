<?php
/**
 * @Author Dmitry Smertin Aka NaggaDIM (naggadim.18@gmail.com)
 * @link https://vk.com/naggadim
 * Date: 24.09.20
 * Time: 10:44
 *
 * Если этот код работает, его написал NaggaDIM,
 * а если нет, то не знаю, кто его писал.
 */
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;

Route::name('auth.')->prefix('/auth')->group(function (){
    Route::post('/', [AuthController::class, 'login'])->name('login');
    Route::post('/register', [AuthController::class, 'register'])->name('register');

    Route::middleware('auth:api')->group(function (){
        Route::delete('/logout', [AuthController::class, 'logout'])->name('logout');
        Route::name('token.')->prefix('/token')->group(function (){
            Route::post('/update', [AuthController::class, 'update_token'])->name('update');
        });
    });
});