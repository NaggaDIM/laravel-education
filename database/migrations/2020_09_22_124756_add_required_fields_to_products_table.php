<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRequiredFieldsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('brand')->nullable(true)->after('id');
            $table->mediumText('description')->nullable(true)->after('name');
            $table->string('category')->nullable(true)->after('description');
            $table->string('image_url')->nullable(true)->after('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('brand');
            $table->dropColumn('description');
            $table->dropColumn('category');
            $table->dropColumn('image_url');
        });
    }
}
