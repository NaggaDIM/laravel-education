<?php
/**
 * @Author Dmitry Smertin Aka NaggaDIM (naggadim.18@gmail.com)
 * @link https://vk.com/naggadim
 * Date: 23.09.20
 * Time: 8:12
 *
 * Если этот код работает, его написал NaggaDIM,
 * а если нет, то не знаю, кто его писал.
 */
namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * ['brand', 'name', 'description', 'category', 'image_url', 'count', 'price']
     */
    public function definition()
    {
        return [
            'brand'         => $this->faker->company,
            'name'          => $this->faker->word,
            'description'   => $this->faker->sentence($nbWords = 20, $variableNbWords = true),
            'category'      => $this->faker->word,
            'image_url'     => $this->faker->imageUrl(),
            'count'         => $this->faker->numberBetween(1, 1000),
            'price'         => $this->faker->randomFloat(2, 1, 1263)
        ];
    }
}