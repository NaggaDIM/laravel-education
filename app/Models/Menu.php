<?php

namespace App\Models;

use App\Rules\RouteExist;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $fillable = ['title', 'route', 'is_auth'];
    protected $casts = ['is_auth' => 'boolean'];

    public function render()
    {
        $elem = "<a href=\"". route($this->route) ."\" class=\"mr-5 hover:text-white\">" . $this->title . "</a>";
        return $this->is_auth ? (\Auth::check() ? $elem : '') : $elem;
    }

    public static function rules()
    {
        return [
            'title' => ['required', 'string', 'max:64'],
            'route' => ['required', 'string', new RouteExist],
        ];
    }


}
