<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function update_token()
    {
        $this->api_token = Str::random(60);
        $this->save();

        return $this->api_token;
    }

    public function reset_token()
    {
        $this->api_token = null;
        $this->save();
    }

    public static function rules($edit = false)
    {
        $rules = [
            'name'      => ['required', 'string'],
            'email'     => ['required', 'email'],
            'password'  => ['regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/i']
        ];

        if(!$edit){
            $rules['email'][] = 'unique:' . self::class . ',email';
            $rules['password'][] = 'required';
        } else {
            $rules['password'][] = 'nullable';
        }

        return $rules;
    }
}
