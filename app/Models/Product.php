<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['brand', 'name', 'description', 'category', 'image_url', 'count', 'price'];

    public static function rules()
    {
        return [
            'brand'         => ['nullable', 'string', 'min:3', 'max:255'],
            'name'          => ['required', 'string', 'min:3', 'max:255'],
            'description'   => ['nullable', 'string', 'min:3', 'max:1000'],
            'category'      => ['nullable', 'string', 'min:3', 'max:255'],
            'image_url'     => ['nullable', 'string', 'min:3', 'max:255'],
            'count'	        => ['required', 'numeric', 'min:1'],
            'price'         => ['required', 'numeric', 'between:1,1263']
        ];
    }
}
