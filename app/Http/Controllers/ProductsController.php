<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facade\Response;
use App\Models\Product;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class ProductsController extends Controller
{
    public function index(Request $request){
        return view('products.list')
            ->with('title', 'Каталог товаров')
            ->with('products', Product::query()->where('count', '>', 0)->paginate(8));
    }

    public function detail(Request $request, $id)
    {
        return view('products.detail')
            ->with('title', 'Информация о товаре')
            ->with('product', Product::findOrFail($id));
    }

    public function add(Request $request)
    {
        return view('products.add')
            ->with('title', 'Добавление товара')
            ->with('prefer', $request->get('prefer', null));
    }

    public function _add(Request $request){
    	$this->validate($request, Product::rules());

    	Product::create([
    		'brand'         => $request->brand,
    		'name'          => $request->name,
    		'description'   => $request->description,
    		'category'      => $request->category,
    		'image_url'     => $request->image_url,
    		'count'         => $request->count,
    		'price'         => $request->price
    	]);

    	return !empty($request->get('prefer', null)) ? Redirect::to($request->prefer) : Redirect::route('products.list');
    }

    public function edit(Request $request, $id)
    {
        return view('products.add')
            ->with('title', 'Добавление товара')
            ->with('product', Product::findOrFail($id))
            ->with('prefer', $request->get('prefer', null));
    }

    public function _edit(Request $request, $id){
        $product = Product::findOrFail($id);
        $this->validate($request, Product::rules());

        $product->update([
            'brand'         => $request->brand,
            'name'          => $request->name,
            'description'   => $request->description,
            'category'      => $request->category,
            'image_url'     => $request->image_url,
            'count'         => $request->count,
            'price'         => $request->price
        ]);

        return !empty($request->get('prefer', null)) ? Redirect::to($request->prefer) : Redirect::route('products.list');
    }
}
