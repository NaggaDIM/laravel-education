<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class MainController extends Controller
{
    /**
     * Главная страница админ панели
     * @method GET
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.dashboard')
            ->with('title', 'Админ Панель');
    }

    /**
     * Список пользователей
     * @method GET
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function users(Request $request)
    {
        return view('admin.users.list')
            ->with('title', 'Пользователи')
            ->with('users', User::paginate(10));
    }

    /**
     * Страница добавления пользователя
     * @method GET
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function add_user(Request $request)
    {
        return view('admin.users.add')
            ->with('title', 'Добавление пользователя');
    }

    /**
     * Добавление пользователя
     * @method POST
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function _add_user(Request $request)
    {
        $this->validate($request, array_merge(User::rules(), [
            'api_token' => ['nullable', 'string', 'min:32', 'max:64']
        ]));

        User::create([
            'email' => $request->email,
            'name' => $request->name,
            'password' => Hash::make($request->password),
            'api_token' => !empty($request->api_token) ? $request->api_token : \Str::random(60)
        ]);

        return Redirect::route('admin.users.list');
    }

    /**
     * Страница редактирования пользователя
     * @method GET
     * @param Request $request
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit_user(Request $request, $id)
    {
        return view('admin.users.add')
            ->with('title', 'Редактирование пользователя')
            ->with('user', User::findOrFail($id));
    }

    /**
     * Редактрование пользователя
     * @method PUT
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function _edit_user(Request $request, $id)
    {
        $this->validate($request, array_merge(User::rules(true), [
            'api_token' => ['nullable', 'string', 'min:32', 'max:64']
        ]));

        $user = User::findOrFail($id);
        $user->update([
            'email'     => !empty($request->email) && $user->email != $request->email ? $request->email : $user->email,
            'name'      => $request->name,
            'password'  => !empty($request->password) && !Hash::check($request->password, $user->password) ? Hash::make($request->password) : $user->password,
            'api_token' => !empty($request->api_token) && $user->api_token != $request->api_token ? $request->api_token : $user->api_token
        ]);

        return Redirect::route('admin.users.list');
    }

    /**
     * Удаление пользователя
     * @method GET
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function _delete_user(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return Redirect::back();
    }

    /**
     * Список товаров
     * @method GET
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function products(Request $request)
    {
        return view('admin.products.list')
            ->with('title', 'Товары')
            ->with('products', Product::paginate(10));
    }

    /**
     * Удаление товара
     * @method GET
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function _delete_product(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return Redirect::back();
    }

    /**
     * Список элементов меню
     * @method GET
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function menu(Request $request)
    {
        return view('admin.menu.list')
            ->with('title', 'Меню')
            ->with('menu_items', Menu::paginate(10));
    }

    /**
     * Страница добавления элемента меню
     * @method GET
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function add_menu(Request $request)
    {
        return view('admin.menu.add')
            ->with('title', 'Добавление элемента меню');
    }

    /**
     * Добавление элемента меню
     * @method POST
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function _add_menu(Request $request)
    {
        $this->validate($request, Menu::rules());

        Menu::create([
            'title'     => $request->title,
            'route'     => $request->route,
            'is_auth'   => $request->has('is_auth') && $request->is_auth == 'on'
        ]);

        return Redirect::route('admin.menu.list');
    }

    /**
     * Страница редактирования элемента меню
     * @method GET
     * @param Request $request
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit_menu(Request $request, $id)
    {
        return view('admin.menu.add')
            ->with('title', 'Редактирование элемента меню')
            ->with('menu_item', Menu::findOrFail($id));
    }

    /**
     * Редактирование элемента меню
     * @method PUT
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function _edit_menu(Request $request, $id)
    {
        $this->validate($request, Menu::rules());

        $menu_item = Menu::findOrFail($id);
        $menu_item->update([
            'title'     => $request->title,
            'route'     => $request->route,
            'is_auth'   => $request->has('is_auth') && $request->is_auth == 'on'
        ]);

        return Redirect::route('admin.menu.list');
    }

    /**
     * Удаление элемента меню
     * @method GET
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function _delete_menu(Request $request, $id)
    {
        $menu_item = Menu::findOrFail($id);
        $menu_item->delete();

        return Redirect::route('admin.menu.list');
    }
}
