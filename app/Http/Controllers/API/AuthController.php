<?php
namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'     => ['required', 'email'],
            'password'  => ['required', 'string']
        ]);

        if(Auth::attempt($request->only('email', 'password'))){
            $user = User::whereEmail($request->email)->first();
        }

        if(!empty($user)){
            if(!empty($user->api_token)){ return Response::json(['token' => $user->api_token]); }
            else {
                return Response::json(['token' => $user->update_token()]);
            }
        } else {
            return Response::json(['message' => 'Invalid Email or Password'], 401);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->reset_token();

        return Response::json(['message' => 'Success Logout']);
    }

    public function register(Request $request)
    {

        $this->validate($request, User::rules());

        User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => Hash::make($request->password),
            'api_token' => Str::random(60)
        ]);

        return Response::json(['message' => 'Success registration']);
    }

    public function update_token(Request $request)
    {
        return Response::json(['token' => $request->user()->update_token()]);
    }
}
