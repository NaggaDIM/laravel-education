<?php
/**
 * @Author Dmitry Smertin Aka NaggaDIM (naggadim.18@gmail.com)
 * @link https://vk.com/naggadim
 * Date: 23.09.20
 * Time: 9:14
 *
 * Если этот код работает, его написал NaggaDIM,
 * а если нет, то не знаю, кто его писал.
 */
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller {

    public function login(Request $request)
    {
        $this->validate($request, [
            'email'     => ['required', 'string', 'email', 'exists:users,email'],
            'password'  => ['required', 'string']
        ]);

        if(!Auth::attempt($request->only('email', 'password'))){
            return Redirect::back()->withInput()->withErrors(['email' => 'Неверный логин или пароль!']);
        }

        Auth::login(User::whereEmail($request->email)->first(), true);
        return Redirect::back();
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return Redirect::route('home');
    }
}